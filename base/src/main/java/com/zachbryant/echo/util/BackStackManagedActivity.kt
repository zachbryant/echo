package com.zachbryant.echo.util

import androidx.annotation.CallSuper
import com.zachbryant.echo.ui.base.BaseActivity
import com.zachbryant.echo.util.BackStackBehavior.*
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber
import java.util.*

@Deprecated("Use fragments instead")
abstract class BackStackManagedActivity<ScreenType> : BaseActivity() {
    private val backStack = Stack<ScreenType>()

    private val backStackObservable = BehaviorSubject.create<ScreenType>()
    protected val backStackScreenEmitter: Flowable<ScreenType> =
        backStackObservable.toFlowable(BackpressureStrategy.BUFFER)

    private val backStackBackArrowObservable = BehaviorSubject.createDefault(false)
    protected val backStackBackArrowEmitter: Flowable<Boolean> =
        backStackBackArrowObservable.toFlowable(BackpressureStrategy.LATEST)

    protected fun addToBackStack(screenIdRes: Int, backStackBehavior: BackStackBehavior) {
        addToBackStack(getScreenTypeFromId(screenIdRes), backStackBehavior)
    }

    protected fun addToBackStack(screenType: ScreenType, backStackBehavior: BackStackBehavior = REPLACE) {
        when (backStackBehavior) {
            REPLACE -> {
                backStack.remove(screenType)
                backStack.push(screenType)
            }
            ADD -> backStack.push(screenType)
            NO_OP -> {
            } // No-op
            REMOVE -> backStackObservable.onError(IllegalArgumentException("BackStackBehavior.REMOVE used in addToBackStack method call."))
        }
        notifyObservers(backStackBehavior)
    }

    private fun notifyObservers(backStackBehavior: BackStackBehavior) {
        when (backStackBehavior) {
            NO_OP -> {
            }
            else -> {
                val peekScreen = peekBackStack()
                if (peekScreen != null) backStackObservable.onNext(peekScreen)
                backStackBackArrowObservable.onNext(backStack.size > 1)
            }
        }
    }

    private fun peekBackStack(): ScreenType? = if (backStack.isEmpty()) null else backStack.peek()

    private fun popBackStack() = backStack.isNotEmpty().also {
        if (it) backStack.pop()
        notifyObservers(REMOVE)
    }

    @CallSuper
    override fun onBackPressed() {
        super.onBackPressed()
        if (backStack.size < 2) {
            Timber.d("Should exit")
            finish()
        }
        else {
            popBackStack()
        }
    }

    // Used to map menu IDs to ScreenType enums
    abstract fun getScreenTypeFromId(id: Int): ScreenType
}