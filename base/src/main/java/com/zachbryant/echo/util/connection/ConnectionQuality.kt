package com.zachbryant.echo.util.connection

enum class ConnectionQuality {
    /**
     * Bandwidth under 150 kbps.
     */
    POOR,
    /**
     * Bandwidth between 150 and 550 kbps.
     */
    MODERATE,
    /**
     * Bandwidth between 550 and 2000 kbps.
     */
    GOOD,
    /**
     * EXCELLENT - Bandwidth over 2000 kbps.
     */
    EXCELLENT,
    /**
     * Placeholder for unknown bandwidth. This is the initial value and will stay at this value
     * if a bandwidth cannot be accurately found.
     */
    UNKNOWN;

    companion object {
        fun valueOf(kbps: Int): ConnectionQuality {
            return when (kbps) {
                in 0 until 150 -> POOR
                in 150 until 550 -> MODERATE
                in 550 until 2000 -> GOOD
                in 2000 until Int.MAX_VALUE -> EXCELLENT
                else -> UNKNOWN
            }
        }
    }
}