package com.zachbryant.echo.util

import com.zachbryant.echo.util.event.ConnectionStateEvent

interface ComponentLifecycle : SubscriptionManagerContract {
    fun onStart()
    fun onStop()
    fun onRefresh()
    fun onError(throwable: Throwable)
    fun onConnectionChange(connectionState: ConnectionStateEvent)
}