package com.zachbryant.echo.util.event

class LifecycleEvent {
    open class ON_CREATE
    open class ON_START
    open class ON_RESUME
    open class ON_PAUSE
    open class ON_STOP
    open class ON_DESTROY
    open class ON_REFRESH
    open class ON_RESTART
}