package com.zachbryant.echo.util.connection

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.os.Build
import com.isupatches.wisefy.WiseFy
import com.zachbryant.echo.util.SubscriptionManager
import com.zachbryant.echo.util.event.ConnectionStateEvent
import com.zachbryant.echo.util.event.EventBus
import com.zachbryant.echo.util.event.LifecycleEvent
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.util.concurrent.TimeUnit

class ConnectivityStatus(private val context: Context) : KoinComponent {
    val NETWORK_STATUS_CHECK_INTERVAL: Long = 5
    private val wisefy = WiseFy.Brains(context).getSmarts()
    private val subscriptionManager: SubscriptionManager by inject()
    private val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    private var shouldBroadcast: Boolean = false

    private val networkTimerObservable =
        Observable.interval(NETWORK_STATUS_CHECK_INTERVAL, TimeUnit.SECONDS).observeOn(Schedulers.single())

    init {
        EventBus.on<LifecycleEvent.ON_START> { subscribeToNetworkChanges() }
        EventBus.on<LifecycleEvent.ON_REFRESH> { broadcast() }
        EventBus.on<LifecycleEvent.ON_STOP> { disposeNetworkChangeSubscription() }
    }

    private fun subscribeToNetworkChanges() {
        networkTimerObservable
            .subscribe { broadcast() }
            .also { subscriptionManager.addDisposable(it) }
    }

    private fun broadcast() {
        if (!isConnected() && !shouldBroadcast) {
            EventBus.post(ConnectionStateEvent.DISCONNECTED)
        }
        if (isConnected() && shouldBroadcast) {
            val quality = when (getQuality()) {
                ConnectionQuality.POOR, ConnectionQuality.MODERATE -> ConnectionStateEvent.CONNECTED_SLOW
                else -> ConnectionStateEvent.CONNECTED
            }
            EventBus.post(quality)
        }
        shouldBroadcast = shouldBroadcast.not()
    }

    private fun disposeNetworkChangeSubscription() = subscriptionManager.dispose()

    fun getNetwork(): Network? {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            connectivityManager.activeNetwork
        } else {
            null // TODO
        }
    }

    fun getCapabilities(network: Network? = getNetwork()): NetworkCapabilities? {
        return if (network != null) connectivityManager.getNetworkCapabilities(network) else null
    }

    fun isConnected() = wisefy.isDeviceConnectedToMobileOrWifiNetwork()
    fun isConnectedToWifi() = wisefy.isDeviceConnectedToWifiNetwork()
    fun isConnectedToMobile() = wisefy.isDeviceConnectedToMobileNetwork()
    fun isMetered() = connectivityManager.isActiveNetworkMetered
    fun getQuality(): ConnectionQuality {
        val capabilities = getCapabilities()
        var kbps: Int = -1
        if (capabilities != null) kbps = capabilities.linkDownstreamBandwidthKbps
        return ConnectionQuality.valueOf(kbps)
    }
}