package com.zachbryant.echo.util

import android.os.Bundle

interface ActivityPresenterLifecycle : SubscriptionManagerContract {

    fun onCreate()
    fun onStart()
    fun onRefresh()
    fun onResume()
    fun onStop()
    fun onPause()
    fun onSaveInstanceState(bundle: Bundle)
    fun onError(throwable: Throwable)
}