package com.zachbryant.echo.util.event

import com.zachbryant.echo.util.SubscriptionManager
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import org.koin.core.KoinComponent
import org.koin.core.inject

class EventBus {

    companion object : KoinComponent {
        private val globalSubscriptionManager: SubscriptionManager by inject()
        val publisher: PublishSubject<Any> = PublishSubject.create()

        inline fun <reified T> on(): Observable<T> = publisher.filter { it is T }.map { it as T }
        inline fun <reified T> on(noinline onNext: (T) -> Unit): Disposable =
            on<T>().subscribe(onNext).also { addDisposable(it) }

        inline fun <reified T> on(noinline onNext: (T) -> Unit, noinline onError: (Throwable) -> Unit): Disposable =
            on<T>().subscribe(onNext, onError).also { addDisposable(it) }

        inline fun <reified T> on(
            noinline onNext: (T) -> Unit,
            noinline onError: (Throwable) -> Unit,
            noinline onComplete: () -> Unit
        ): Disposable = on<T>().subscribe(onNext, onError, onComplete).also { addDisposable(it) }

        fun post(event: Any, onNext: Boolean = true, onError: Throwable? = null, onComplete: Boolean = false) {
            if (onNext) publisher.onNext(event)
            if (onError != null) publisher.onError(onError)
            if (onComplete) publisher.onComplete()
        }

        fun addDisposable(disposable: Disposable) = globalSubscriptionManager.addDisposable(disposable)
        private fun dispose() = globalSubscriptionManager.dispose()
    }
}