package com.zachbryant.echo.util

enum class BackStackBehavior {
    ADD, REPLACE, REMOVE, NO_OP
}