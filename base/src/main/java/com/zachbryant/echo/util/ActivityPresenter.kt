package com.zachbryant.echo.util

interface ActivityPresenter<ScreenType> : ActivityPresenterLifecycle {
    fun renderScreen(screenType: ScreenType)
}