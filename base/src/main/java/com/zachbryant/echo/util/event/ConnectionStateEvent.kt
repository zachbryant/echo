package com.zachbryant.echo.util.event

class ConnectionStateEvent {
    open class CONNECTED
    open class DISCONNECTED
    open class CONNECTED_SLOW
}