package com.zachbryant.echo.util.event

class DeviceEvent {
    open class ON_LOW_MEMORY
    open class ON_BACK_PRESSED
}