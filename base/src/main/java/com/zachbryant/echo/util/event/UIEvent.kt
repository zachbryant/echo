package com.zachbryant.echo.util.event

import android.content.Context
import androidx.core.content.ContextCompat
import org.koin.core.KoinComponent
import org.koin.core.inject

class UIEvent {

    class UPDATE_TOOLBAR_TITLE() : KoinComponent {
        private val context: Context by inject()
        var title: String = ""

        constructor(title: String) : this() {
            this.title = title
        }

        constructor(titleResId: Int) : this() {
            this.title = context.getString(titleResId)
        }
    }

    class UPDATE_ACCENT_COLOR(colorInt: Int, resId: Boolean = false) : KoinComponent {
        private val context: Context by inject()
        val color = if (resId) ContextCompat.getColor(context, colorInt) else colorInt
    }
}