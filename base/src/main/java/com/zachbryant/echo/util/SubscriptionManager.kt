package com.zachbryant.echo.util

import com.zachbryant.echo.util.event.EventBus
import com.zachbryant.echo.util.event.LifecycleEvent
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable


class SubscriptionManager : Disposable {
    private var _compositeDisposable = CompositeDisposable()
    private val compositeDisposable: CompositeDisposable
        get() {
           if(_compositeDisposable.isDisposed) _compositeDisposable = CompositeDisposable()
            return _compositeDisposable
        }

    init {
        EventBus.on<LifecycleEvent.ON_STOP> { dispose() }
    }

    override fun isDisposed(): Boolean = _compositeDisposable.isDisposed

    override fun dispose() = _compositeDisposable.dispose()

    fun addDisposable(disposable: Disposable) = compositeDisposable.add(disposable)
}