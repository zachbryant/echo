package com.zachbryant.echo.util

import io.reactivex.disposables.Disposable

interface SubscriptionManagerContract {

    var subscriptionManager: SubscriptionManager

    fun addDisposable(disposable: Disposable) = subscriptionManager.addDisposable(disposable)
    fun dispose() = subscriptionManager.dispose()
    fun isDisposed() = subscriptionManager.isDisposed
}