package com.zachbryant.echo.prefs

import com.marcinmoskala.kotlinpreferences.PreferenceHolder
import java.lang.Boolean.TRUE

object UserPref : PreferenceHolder() {
    var dayNightMode: Boolean by bindToPreferenceField(TRUE)
}