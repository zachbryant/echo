package com.zachbryant.echo.koin

import com.zachbryant.echo.koin.modules.applicationModule
import org.koin.core.module.Module

@JvmField
val koinGraph = listOf<Module>(
    applicationModule
)