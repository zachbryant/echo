package com.zachbryant.echo.koin.modules

import android.content.Context
import android.content.res.Resources
import com.zachbryant.echo.util.SubscriptionManager
import org.koin.dsl.module

val applicationModule = module {
    single<SubscriptionManager> {
        SubscriptionManager()
    }

    single<Resources> {
        val context: Context by inject()
        return@single context.resources
    }
}