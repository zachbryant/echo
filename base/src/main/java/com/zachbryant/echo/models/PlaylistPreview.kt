package com.zachbryant.echo.models

import com.squareup.moshi.Json

data class PlaylistPreview(
    @Json(name = "title") @field:Json(name = "title") val title: String,
    @Json(name = "description") @field:Json(name = "description") val description: String,
    @Json(name = "tags") @field:Json(name = "tags") val tags: List<String>,
    @Json(name = "playlist_uri") @field:Json(name = "playlist_uri") val playlistUrl: String,
    @Json(name = "image_uri") @field:Json(name = "image_uri") val imageUri: String,
    @Json(name = "is_favorite") @field:Json(name = "is_favorite") val isFavorite: Boolean
)