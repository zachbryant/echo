package com.zachbryant.echo.ui.screen.dashboard

import com.zachbryant.echo.R
import com.zachbryant.echo.ui.base.BaseComponent
import com.zachbryant.echo.util.event.ConnectionStateEvent
import com.zachbryant.echo.util.event.EventBus
import com.zachbryant.echo.util.event.UIEvent
import org.koin.core.KoinComponent

class DashboardComponent : BaseComponent(), KoinComponent {
    override fun onConnectionChange(connectionState: ConnectionStateEvent) {
        TODO("not implemented")
    }


    init {
        addComponent(FeaturedPlaylistsComponent())
    }

    override fun onStart() {
        EventBus.post(UIEvent.UPDATE_TOOLBAR_TITLE(R.string.title_dashboard))
    }

    override fun onRefresh() {
    }

    override fun onStop() {
    }

    override fun onError(throwable: Throwable) {
        TODO("not implemented")
    }
}