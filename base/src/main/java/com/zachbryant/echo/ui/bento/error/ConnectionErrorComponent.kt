package com.zachbryant.echo.ui.bento.error

import android.view.View
import com.yelp.android.bento.componentcontrollers.SimpleComponentViewHolder
import com.yelp.android.bento.components.SimpleComponent
import com.zachbryant.echo.R

class ConnectionErrorComponent : SimpleComponent<Nothing?>(ConnectionErrorComponentViewHolder::class.java) {

    class ConnectionErrorComponentViewHolder : SimpleComponentViewHolder<Nothing?>(R.layout.layout_error_connection) {
        override fun onViewCreated(itemView: View) = Unit
    }
}