package com.zachbryant.echo.ui.base

import com.yelp.android.bento.core.ComponentGroup
import com.zachbryant.echo.util.ComponentLifecycle
import com.zachbryant.echo.util.SubscriptionManager
import com.zachbryant.echo.util.event.EventBus
import com.zachbryant.echo.util.event.LifecycleEvent

abstract class BaseComponent : ComponentLifecycle, ComponentGroup() {

    override var subscriptionManager = SubscriptionManager()

    init {
        EventBus.on<LifecycleEvent.ON_START> { onStart() }
        EventBus.on<LifecycleEvent.ON_REFRESH> { onRefresh() }
        EventBus.on<LifecycleEvent.ON_STOP> { onStop() }
    }
}