package com.zachbryant.echo.ui.bento

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.yelp.android.bento.core.Component
import com.yelp.android.bento.core.ComponentViewHolder
import com.yelp.android.bento.utils.inflate
import com.zachbryant.echo.R

class HeaderComponent(private val text: String) : Component() {

    override fun getItem(position: Int) = text

    override fun getCount() = 1

    override fun getHolderType(position: Int) = HeaderComponentViewHolder::class.java

    override fun getPresenter(position: Int): Nothing? = null

    class HeaderComponentViewHolder : ComponentViewHolder<Nothing?, String>() {

        private lateinit var textView: TextView

        override fun bind(presenter: Nothing?, element: String) {
            textView.text = element
        }

        override fun inflate(parent: ViewGroup) =
            parent.inflate<View>(R.layout.layout_section_header).also { textView = it.findViewById(R.id.textView) }
    }

}