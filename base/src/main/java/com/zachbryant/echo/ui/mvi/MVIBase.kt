package com.zachbryant.echo.ui.mvi

import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject

interface MVIBase {
    abstract class BaseModel<INTENT : Intent, VM> : Model<INTENT, VM> {

        protected val disposables = CompositeDisposable()
        protected val viewModel: BehaviorSubject<VM> = BehaviorSubject.create()
        private val events: PublishSubject<Event> = PublishSubject.create()

        override fun viewModel(): Observable<VM> = viewModel

        override fun attach(intents: Observable<INTENT>) {
            makeViewModel(
                eventsFrom(intents),
                initialViewModel(),
                ::reduce
            )
            disposables.add(sideEffectsFrom(intents))
        }

        override fun detach() {
            disposables.clear()
        }

        /**
         * @return instance of [VM] representing the initial state
         */
        protected abstract fun initialViewModel(): VM

        /**
         * Define state mutation events that result in view model changes
         *
         * NB: Operations that don't cause a state mutation reside in [sideEffectsFrom]
         *
         * @return [Observable] of [Event] feeding into [reduce]
         */
        protected abstract fun eventsFrom(intents: Observable<INTENT>): Observable<out Event>

        /**
         * Reduce state mutations to updated view models
         *
         * NB: Should be a pure function without side effects
         */
        protected abstract fun reduce(model: VM, event: Event): VM

        /**
         * Define side effects that don't result in state mutations, if any
         *
         * Default implementation results in no-op.
         *
         * NB: Returned subscriptions will be added to [disposables] and disposed in [detach]
         *
         * @return [CompositeDisposable] containing subscriptions that need to be managed
         */
        protected open fun sideEffectsFrom(intents: Observable<INTENT>): CompositeDisposable {
            return CompositeDisposable()
        }

        /**
         * Exposes the internal [Event] stream that passes through the reducer
         */
        protected fun events(): Observable<Event> = events

        /**
         * Dispatch an event to the reducer
         */
        fun dispatchEvent(event: Event) = events.onNext(event)

        /**
         * Subscribes internal event relay to supplied event stream and sets up [viewModel]
         */
        private fun makeViewModel(
            events: Observable<out Event>,
            initialViewModel: VM,
            reducer: (VM, Event) -> VM
        ) {
            disposables.add(
                this.events
                    .scan(initialViewModel, reducer)
                    .distinctUntilChanged()
                    .subscribe(viewModel::onNext)
            )
            disposables.add(events.subscribe(this.events::onNext))
        }
    }

    interface View<INTENT : Intent, VM> {
        /**
         * Attach the view to a view model stream
         *
         * Usage:
         * <code>
         *     view.attach(model.viewModel().observeOn(AndroidSchedulers.mainThread()))
         * </code>
         */
        fun attach(viewModel: Observable<VM>)

        /**
         * Obtain intent stream for attaching to model
         *
         * Usage:
         * <code>
         *     model.attach(view.intents())
         * </code>
         */
        fun intents(): Observable<INTENT>
    }

    interface Model<INTENT : Intent, VM> {
        /**
         * Attach the model to an intent stream
         *
         * NB: Always attach model to view before attaching intent stream to model.
         *
         * Usage:
         * <code>
         *     override fun onCreate(savedInstanceState: Bundle?) {
         *         super.onCreate(savedInstanceState)
         *         view.attach(model.viewModel().observeOn(AndroidSchedulers.mainThread()))
         *         model.attach(view.intents())
         *     }
         * </code>
         */
        fun attach(intents: Observable<INTENT>)

        /**
         * Detach the model from previously attached intent stream
         *
         * Usage:
         * <code>
         *     override fun onDestroy() {
         *         model.detach()
         *         super.onDestroy()
         *     }
         * </code>
         */
        fun detach()

        /**
         * ViewModel stream
         *
         * NB: Observe on main thread
         */
        fun viewModel(): Observable<VM>
    }

    /**
     * The model uses a reducer function that produces an updated view model
     * based on the previous view model and a state mutation event.
     * <code>
     *     reducer: (VM, EVENT) -> VM
     * </code>
     * State mutations are internal to the model and are not exposed to the view.
     */
    interface Event

    /**
     * Intents flow from the view to the model.
     *
     * Example:
     * In order to fetch data for a screen, the view would send a
     * corresponding intent to the model, specifying the desired data.
     */
    interface Intent
}