package com.zachbryant.echo.ui.bento

import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Space
import androidx.annotation.Px
import com.yelp.android.bento.core.Component
import com.yelp.android.bento.core.ComponentViewHolder

class GapComponent(@Px val height: Int) : Component() {

    override fun getItem(position: Int) = height

    override fun getCount() = 1

    override fun getHolderType(position: Int) = GapComponentViewHolder::class.java

    override fun getPresenter(position: Int): Nothing? = null

    class GapComponentViewHolder : ComponentViewHolder<Nothing?, Int>() {

        private lateinit var view: View

        override fun bind(presenter: Nothing?, element: Int) {
            view.layoutParams = FrameLayout.LayoutParams(element, element)
        }

        override fun inflate(parent: ViewGroup) = Space(parent.context).also(::view::set)

    }
}