package com.zachbryant.echo.ui

import android.os.Bundle
import com.yelp.android.bento.core.ComponentGroup
import com.zachbryant.echo.ui.base.BaseComponent
import com.zachbryant.echo.ui.screen.dashboard.DashboardComponent
import com.zachbryant.echo.ui.screen.help.HelpComponent
import com.zachbryant.echo.ui.screen.library.LibraryComponent
import com.zachbryant.echo.ui.screen.login.LoginComponent
import com.zachbryant.echo.ui.screen.onboarding.OnboardingComponent
import com.zachbryant.echo.ui.screen.search.SearchComponent
import com.zachbryant.echo.ui.screen.settings.SettingsComponent
import com.zachbryant.echo.util.ActivityPresenter
import com.zachbryant.echo.util.SubscriptionManager
import timber.log.Timber
import java.net.SocketTimeoutException

class MainPresenter : ActivityPresenter<MainActivity.Screen>, ComponentGroup() {

    override var subscriptionManager = SubscriptionManager()
    val DEFAULT_SCREEN: MainActivity.Screen =
        MainActivity.Screen.DASHBOARD
    private var activeScreen: BaseComponent? = null
        set(value) {
            if (value == null) throw IllegalArgumentException("Presenter's Controller received a null component.")
            else if (value != activeScreen) {
                if (size == 0) addComponent(value) else replaceComponent(0, value)
                value.onStart()
                field?.onStop()
                field = value
            }
        }
        get() = if (size > 0) get(0) as BaseComponent else null

    // "Screen" components lazy loaded because some may never be in use
    private val dashboardScreen: DashboardComponent by lazy {
        DashboardComponent().also { addDisposable(it.subscriptionManager) }
    }
    private val libraryScreen: LibraryComponent by lazy {
        LibraryComponent().also { addDisposable(it.subscriptionManager) }
    }
    private val helpScreen: HelpComponent by lazy {
        HelpComponent().also { addDisposable(it.subscriptionManager) }
    }
    private val loginScreen: LoginComponent by lazy {
        LoginComponent().also { addDisposable(it.subscriptionManager) }
    }
    private val onboardingScreen: OnboardingComponent by lazy {
        OnboardingComponent().also { addDisposable(it.subscriptionManager) }
    }
    private val searchScreen: SearchComponent by lazy {
        SearchComponent().also { addDisposable(it.subscriptionManager) }
    }
    private val settingsScreen: SettingsComponent by lazy {
        SettingsComponent().also { addDisposable(it.subscriptionManager) }
    }

    override fun renderScreen(screenType: MainActivity.Screen) {
        Timber.d(screenType.toString())
        activeScreen = when (screenType) {
            MainActivity.Screen.DASHBOARD -> dashboardScreen
            MainActivity.Screen.LIBRARY -> libraryScreen
            MainActivity.Screen.HELP -> helpScreen
            MainActivity.Screen.LOGIN -> loginScreen
            MainActivity.Screen.ONBOARDING -> onboardingScreen
            MainActivity.Screen.SEARCH -> searchScreen
            MainActivity.Screen.NO_OP -> activeScreen
            MainActivity.Screen.SETTINGS -> settingsScreen
        }
        Timber.d("$size")
    }

    override fun onCreate() = initComponents()

    private fun initComponents() {

    }

    override fun onStart() {
        renderScreen(DEFAULT_SCREEN)
    }

    override fun onResume() {
        activeScreen?.onStart()
    }

    override fun onStop() {
        dispose()
    }

    override fun onPause() {
        activeScreen?.onStop()
    }

    override fun onSaveInstanceState(bundle: Bundle) {
        //activeScreen?.onSaveInstanceState(bundle)
    }

    override fun onRefresh() {
        activeScreen?.onRefresh()
    }

    override fun onError(throwable: Throwable) {
        when (throwable) {
            is SocketTimeoutException -> activeScreen?.onConnectionError()
            else -> activeScreen?.onError(throwable)
        }
    }
}