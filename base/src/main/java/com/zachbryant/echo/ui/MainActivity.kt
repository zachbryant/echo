package com.zachbryant.echo.ui

import android.app.Activity
import android.app.ActivityManager
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.jakewharton.rxbinding3.material.itemSelections
import com.jakewharton.rxbinding3.view.clicks
import com.marcinmoskala.kotlinpreferences.PreferenceHolder
import com.yelp.android.bento.componentcontrollers.RecyclerViewComponentController
import com.zachbryant.echo.R
import com.zachbryant.echo.prefs.UserPref
import com.zachbryant.echo.ui.base.BaseActivity
import com.zachbryant.echo.util.event.DeviceEvent
import com.zachbryant.echo.util.event.EventBus
import com.zachbryant.echo.util.event.UIEvent
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.core.KoinComponent
import timber.log.Timber
import java.util.concurrent.TimeUnit
import kotlin.math.min

class MainActivity : BaseActivity(), OnRefreshListener, KoinComponent {

    enum class Screen { DASHBOARD, LIBRARY, HELP, LOGIN, ONBOARDING, SETTINGS, SEARCH, NO_OP }

    private val toolbarColorChangeOffset = 0.4f
    private var toolbarColor: Int = 0

    override lateinit var componentController: RecyclerViewComponentController
    private val presenter: MainPresenter by lazy { MainPresenter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        PreferenceHolder.setContext(applicationContext)
        renderTheme()
        setContentView(R.layout.activity_main)
        setupToolbar()
        setupBottomBar()
        setupRecentAppsStyle()
        setupRefreshLayout()
        componentController = RecyclerViewComponentController(recyclerView).apply { addComponent(presenter) }
        recyclerView.adapter?.registerAdapterDataObserver(scrollOnInsertFirstItemObserver)

        addDisposable(presenter.subscriptionManager)
        presenter.onCreate()
    }

    private fun setupBottomBar() {
        bottomBar
            .itemSelections()
            .debounce(200, TimeUnit.MILLISECONDS)
            .subscribe(
                {
                    Timber.d("Bottombar")
                    presenter.renderScreen(getScreenTypeFromId(it.itemId))
                },
                {
                    presenter.onError(it)
                }
            )
            .also { presenter.addDisposable(it) }
    }

    private fun setupRefreshLayout() {
        refreshLayout.apply{
            setColorSchemeResources(R.color.colorAccent)
            setProgressBackgroundColorSchemeResource(R.color.background_elevated_one)
            setOnRefreshListener(this@MainActivity)
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(topBar)
        topBar.apply {
            setBackgroundColor(ContextCompat.getColor(this@MainActivity, R.color.transparent_interface))
            setNavigationOnClickListener { onBackPressed() }
        }
        supportActionBar?.apply {
            setDisplayShowTitleEnabled(false)
        }
        EventBus.on<DeviceEvent.ON_BACK_PRESSED> { showBackArrow() }.also { presenter.addDisposable(it) }
        setNavigationAndStatusBarColor(ContextCompat.getColor(this, R.color.colorAccent))
        EventBus.on<UIEvent.UPDATE_TOOLBAR_TITLE>().subscribe { topBarTitle.text = it.title }.also { addDisposable(it) }
        settingsButton.clicks().subscribe {
            UserPref.dayNightMode = UserPref.dayNightMode.not()
            renderTheme()
            //presenter.renderScreen(Screen.SETTINGS)
        }.also { addDisposable(it) }
    }

    private fun setNavigationAndStatusBarColor(color: Int) {
        window.navigationBarColor = color
        window.statusBarColor = window.navigationBarColor
    }

    private fun setupRecentAppsStyle() {
        val activityRecentAppsDescription = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            ActivityManager.TaskDescription(
                getString(R.string.app_name),
                R.mipmap.ic_launcher_round,
                getColor(R.color.colorPrimaryDark)
            )
        } else {
            ActivityManager.TaskDescription(
                getString(R.string.app_name),
                BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher_round),
                ContextCompat.getColor(this@MainActivity, R.color.colorPrimaryDark)
            )
        }
        (this@MainActivity as Activity).setTaskDescription(activityRecentAppsDescription)
    }

    private fun shouldDisplayBackArrow(): Boolean = fragmentManager.backStackEntryCount > 1

    private fun showBackArrow(shouldDisplayBackArrow: Boolean = shouldDisplayBackArrow()) {
        /*supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(shouldDisplayBackArrow)
            setDisplayShowHomeEnabled(shouldDisplayBackArrow)
        }*/
        backButton.visibility = if (shouldDisplayBackArrow) View.VISIBLE else View.GONE
    }

    override fun onStart() {
        super.onStart()
        presenter.onStart()
    }

    override fun onStop() {
        super.onStop()
        presenter.onStop()
    }

    override fun onPause() {
        super.onPause()
        presenter.onPause()
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun onRefresh() {
        presenter.onRefresh()
        refreshLayout.isRefreshing = false
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        presenter.onSaveInstanceState(outState)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        //menuInflater.inflate(R.menu.toolbar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_theme_toggle -> {
                UserPref.dayNightMode = UserPref.dayNightMode.not()
                renderTheme()
                Screen.NO_OP
            }
            R.id.action_login -> Screen.LOGIN
            R.id.action_help -> Screen.HELP
            R.id.action_walkthrough -> Screen.ONBOARDING
            else -> Screen.NO_OP
        }.also(presenter::renderScreen).also { Timber.d("onOptionsItemSelected") }
        return true
    }

    private fun renderTheme() {
        val dayNightModeInt =
            if (UserPref.dayNightMode) AppCompatDelegate.MODE_NIGHT_YES else AppCompatDelegate.MODE_NIGHT_NO
        AppCompatDelegate.setDefaultNightMode(dayNightModeInt)
        toolbarColor = getToolbarColorVariant()
    }

    private fun getToolbarColorVariant(): Int {
        val typedValue = TypedValue()
        theme.resolveAttribute(R.attr.colorPrimary, typedValue, true)
        return ContextCompat.getColor(this, typedValue.resourceId)
    }

    private fun updateToolbarOpacity(percentage: Float) {
        if (percentage >= toolbarColorChangeOffset) {
            val opacity: Int = min((percentage * 255).toInt(), 255)
            val calculatedToolbarColor = ColorUtils.setAlphaComponent(toolbarColor, opacity)
            topBar.setBackgroundColor(calculatedToolbarColor)
        }
    }

    fun getScreenTypeFromId(id: Int) = when (id) {
        R.id.navigation_dashboard -> Screen.DASHBOARD
        R.id.navigation_search -> Screen.SEARCH
        R.id.navigation_library -> Screen.LIBRARY
        R.id.action_login -> Screen.LOGIN
        R.id.action_help -> Screen.HELP
        R.id.action_walkthrough -> Screen.ONBOARDING
        R.id.settingsButton -> Screen.SETTINGS
        else -> Screen.NO_OP
    }

    private val scrollOnInsertFirstItemObserver = object : RecyclerView.AdapterDataObserver() {
        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
            val layoutManager = recyclerView.layoutManager as LinearLayoutManager
            val firstVisiblePosition = layoutManager.findFirstVisibleItemPosition()
            if (firstVisiblePosition == positionStart && firstVisiblePosition == 0)
                recyclerView.post { layoutManager.scrollToPosition(0) }
        }
    }
}
