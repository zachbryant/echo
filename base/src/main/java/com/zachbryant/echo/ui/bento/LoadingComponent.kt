package com.zachbryant.echo.ui.bento

import android.view.View
import com.yelp.android.bento.componentcontrollers.SimpleComponentViewHolder
import com.yelp.android.bento.components.SimpleComponent
import com.zachbryant.echo.R

class LoadingComponent : SimpleComponent<Nothing?>(LoadingComponentViewHolder::class.java) {

    class LoadingComponentViewHolder : SimpleComponentViewHolder<Nothing?>(R.layout.layout_loading) {
        override fun onViewCreated(itemView: View) = Unit
    }
}