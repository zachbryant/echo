package com.zachbryant.echo.ui.bento.error

import android.view.View
import com.yelp.android.bento.componentcontrollers.SimpleComponentViewHolder
import com.yelp.android.bento.components.SimpleComponent
import com.zachbryant.echo.R

class GenericErrorComponent : SimpleComponent<Nothing?>(GenericErrorComponentViewHolder::class.java) {

    class GenericErrorComponentViewHolder : SimpleComponentViewHolder<Nothing?>(R.layout.layout_error) {
        override fun onViewCreated(itemView: View) = Unit
    }
}