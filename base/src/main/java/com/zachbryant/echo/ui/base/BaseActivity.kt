package com.zachbryant.echo.ui.base

import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import com.yelp.android.bento.core.ComponentController
import com.zachbryant.echo.BuildConfig
import com.zachbryant.echo.koin.koinGraph
import com.zachbryant.echo.util.SubscriptionManager
import com.zachbryant.echo.util.event.DeviceEvent
import com.zachbryant.echo.util.event.EventBus
import com.zachbryant.echo.util.event.LifecycleEvent
import io.reactivex.disposables.Disposable
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import timber.log.Timber


abstract class BaseActivity : AppCompatActivity() {

    protected abstract val componentController: ComponentController
    protected val fragmentManager: FragmentManager = supportFragmentManager
    private val subscriptionManager: SubscriptionManager by inject()

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        Timber.i("#onCreate")
        startKoin {
            androidLogger()
            androidContext(this@BaseActivity)
            modules(koinGraph)
        }
    }

    @CallSuper
    override fun onLowMemory() {
        super.onLowMemory()
        EventBus.post(object : DeviceEvent.ON_LOW_MEMORY(){})
        Timber.i("#onLowMemory")
    }

    @CallSuper
    override fun onRestart() {
        super.onRestart()
        EventBus.post(object : LifecycleEvent.ON_RESTART(){})
        Timber.i("#onRestart")
    }

    @CallSuper
    override fun onStart() {
        super.onStart()
        EventBus.post(object : LifecycleEvent.ON_START(){})
        Timber.i("#onStart")
    }

    @CallSuper
    override fun onResume() {
        super.onResume()
        EventBus.post(object : LifecycleEvent.ON_RESUME(){})
        Timber.i("#onResume")
    }

    @CallSuper
    override fun onBackPressed() {
        super.onBackPressed()
        EventBus.post(object : DeviceEvent.ON_BACK_PRESSED(){})
        Timber.i("#onBackPressed")
    }

    @CallSuper
    override fun onPause() {
        super.onPause()
        EventBus.post(object : LifecycleEvent.ON_PAUSE(){})
        Timber.i("#onPause")
    }

    @CallSuper
    override fun onStop() {
        super.onStop()
        EventBus.post(object : LifecycleEvent.ON_STOP(){})
        Timber.i("#onStop")
        subscriptionManager.dispose()
    }

    @CallSuper
    override fun onDestroy() {
        super.onDestroy()
        EventBus.post(object : LifecycleEvent.ON_DESTROY(){})
        Timber.i("#onDestroy")
        Timber.uprootAll()
        stopKoin()
    }

    fun addDisposable(disposable: Disposable) = subscriptionManager.addDisposable(disposable)
}