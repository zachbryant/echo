package com.zachbryant.echo.ui.screen.dashboard

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.jakewharton.rxbinding3.view.clicks
import com.yelp.android.bento.core.Component
import com.yelp.android.bento.core.ComponentViewHolder
import com.yelp.android.bento.utils.inflate
import com.zachbryant.echo.R
import com.zachbryant.echo.models.PlaylistPreview
import java.util.concurrent.TimeUnit

class PlaylistCardComponent(val presenter: FeaturedPlaylistsComponent, val playlistInfo: PlaylistPreview) :
    Component() {

    override fun getItem(position: Int): PlaylistPreview = playlistInfo

    override fun getCount() = 1

    override fun getHolderType(position: Int) = PlaylistCardComponentViewHolder::class.java

    override fun getPresenter(position: Int) = presenter

    class PlaylistCardComponentViewHolder : ComponentViewHolder<FeaturedPlaylistsComponent, PlaylistPreview>() {

        private lateinit var view: View
        private lateinit var title: TextView
        private lateinit var subtext: TextView
        private lateinit var backgroundImage: ImageView

        override fun bind(presenter: FeaturedPlaylistsComponent, element: PlaylistPreview) {
            view
                .clicks()
                .debounce(1, TimeUnit.SECONDS)
                .subscribe { presenter.onPlaylistPreviewClicked(element) }
                .also { presenter.addDisposable(it) }
            title.text = element.title
            subtext.text = element.description
            if (element.imageUri.isEmpty()) {
                Glide.with(view).load(when((1..2).random()) {
                    1 -> R.drawable.playlist_ex_1
                    2 -> R.drawable.playlist_ex_2
                    else -> R.drawable.playlist_ex_2
                }).into(backgroundImage)
            } else {
                Glide.with(view)
                    .load(element.imageUri)
                    .into(backgroundImage)
            }

        }

        override fun inflate(parent: ViewGroup): View {
            return parent
                .inflate<View>(R.layout.item_playlist_card)
                .also {
                    view = it
                    title = it.findViewById(R.id.playlist_title)
                    subtext = it.findViewById(R.id.playlist_subtext)
                    backgroundImage = it.findViewById(R.id.background)
                }
        }

    }
}