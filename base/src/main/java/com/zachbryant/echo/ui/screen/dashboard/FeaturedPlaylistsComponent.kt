package com.zachbryant.echo.ui.screen.dashboard

import android.content.Context
import com.zachbryant.echo.R
import com.zachbryant.echo.models.PlaylistPreview
import com.zachbryant.echo.ui.base.BaseComponent
import com.zachbryant.echo.ui.bento.HeaderComponent
import com.zachbryant.echo.util.event.ConnectionStateEvent
import org.koin.core.KoinComponent
import org.koin.core.inject
import timber.log.Timber

class FeaturedPlaylistsComponent : BaseComponent(), KoinComponent {
    override fun onConnectionChange(connectionState: ConnectionStateEvent) {
        TODO("not implemented")
    }

    private val context: Context by inject()
    private val playlistCarousel = PlaylistCarouselComponent()

    init {
        val title = context.getString(R.string.title_featured)
        addComponent(HeaderComponent(title))
        addComponent(playlistCarousel)
    }

    override fun onStart() {
        load()
    }

    fun onFavoriteButtonClicked(element: PlaylistPreview) {
        TODO()
    }

    private fun load() {
        val preview = PlaylistPreview("Seaside Servers", "Keyboard clacks, calm waves, server fans", listOf(), "", "", true)
        playlistCarousel.addAll(listOf(
            PlaylistCardComponent(this, preview),
            PlaylistCardComponent(this, preview),
            PlaylistCardComponent(this, preview),
            PlaylistCardComponent(this, preview),
            PlaylistCardComponent(this, preview)
        ))
        playlistCarousel.notifyDataChanged()
    }

    override fun onStop() {
        //TODO("not implemented")
    }

    override fun onRefresh() {
        //TODO("not implemented")
    }

    override fun onError(throwable: Throwable) {
        //TODO("not implemented")
    }

    fun onPlaylistPreviewClicked(element: PlaylistPreview) {
        Timber.d("Playlist preview element clicked")
        //TODO("not implemented")
    }
}