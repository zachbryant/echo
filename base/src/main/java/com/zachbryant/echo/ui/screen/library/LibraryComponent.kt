package com.zachbryant.echo.ui.screen.library

import com.zachbryant.echo.R
import com.zachbryant.echo.ui.base.BaseComponent
import com.zachbryant.echo.util.event.ConnectionStateEvent
import com.zachbryant.echo.util.event.EventBus
import com.zachbryant.echo.util.event.UIEvent
import org.koin.core.KoinComponent

class LibraryComponent : BaseComponent(), KoinComponent {
    override fun onConnectionChange(connectionState: ConnectionStateEvent) {
        TODO("not implemented")
    }

    override fun onStart() {
        EventBus.post(UIEvent.UPDATE_TOOLBAR_TITLE(R.string.title_library))
    }

    override fun onRefresh() {
        TODO("not implemented")
    }

    override fun onStop() {
        //TODO("not implemented")
    }

    override fun onError(throwable: Throwable) {
        TODO("not implemented")
    }
}